//
//  AppDelegate.swift
//  CopCam
//
//  Created by Nidhi Sharma on 7/13/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, ServerWrapperDelegate {

    enum Actions:String{
        case completed = "COMPLETED_ACTION"
        case remindMe = "REMIND_ME_ACTION"
    }
    
    var categoryID:String {
        get{
            return "CATEGORY_COPCAM"
        }
    }
    
    let backend = ServerWrapper()
    var window: UIWindow?
    var isInvokedFromPush = Int()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        setRootController()
        registerForPush()
        window?.makeKeyAndVisible()
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication)
    {
        Helper.GetAppDelegate().isInvokedFromPush = 0
    }

    func applicationWillEnterForeground(application: UIApplication)
    {
        setRootController()
        registerForPush()
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func registerForPushNotifications()
    {
        // ---------------- create actions ------------------
        
        // completed Action
        let completedAction = UIMutableUserNotificationAction()
        completedAction.identifier = Actions.completed.rawValue
        completedAction.title = "Completed"
        completedAction.activationMode = UIUserNotificationActivationMode.Background
        completedAction.authenticationRequired = true
        completedAction.destructive = false
        
        // Remind Me 
        let remindMe = UIMutableUserNotificationAction()
        remindMe.identifier = Actions.remindMe.rawValue
        remindMe.title = "Remind Me"
        remindMe.activationMode = UIUserNotificationActivationMode.Foreground
        remindMe.destructive = false
        
        // create category
        let category = UIMutableUserNotificationCategory()
        category.identifier = categoryID
        
        // actions for default context (when show the options in dialog)
        category.setActions([completedAction, remindMe], forContext: UIUserNotificationActionContext.Default)
        
        // actions for minomal context
        category.setActions([completedAction, remindMe], forContext: UIUserNotificationActionContext.Minimal)
        
        // notification registration
        let types = UIUserNotificationType.Alert | UIUserNotificationType.Sound | UIUserNotificationType.Badge
        let settings = UIUserNotificationSettings(forTypes: types, categories: NSSet(object: category) as Set<NSObject>)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
    }
    
    // MARK - Push notification delegate methods
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings)
    {
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError)
    {
        //generateLocalNotification() // test
        NSLog("didFailToRegisterForRemoteNotificationsWithError")
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData)
    {
        //generateLocalNotification() // test
        
        if Reachability.reachabilityForInternetConnection().isReachable() == true
        {
            Helper.GetAppDelegate().backend.sendMessageToken(deviceToken, delegate: self)
        }
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        NSLog("didReceiveRemoteNotification %@", userInfo)
    }
    
    // test
    func generateLocalNotification()
    {
//        let notification = UILocalNotification()
//        notification.alertBody = "Test Notification.."
//        notification.fireDate = NSDate()
//        notification.category = "ACTION_CATEGORY"
//        notification.repeatInterval = NSCalendarUnit.CalendarUnitMinute
//        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }
    // test
    
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forLocalNotification notification: UILocalNotification, completionHandler: () -> Void)
    {
        // handle notification action
        if notification.category == categoryID
        {
            let action:Actions = Actions(rawValue: identifier!)!
            switch action
            {
                case Actions.remindMe:
                    NSLog("Inside Remind me action")
                    self.isInvokedFromPush = 1
                    var settingsScreen = Settings(nibName: "Settings", bundle: nil)
                    var navigationContoller = UINavigationController(rootViewController: settingsScreen)
                    window?.rootViewController = navigationContoller
                case Actions.completed:
                    NSLog("Inside Completed action")
                    self.isInvokedFromPush = 2
                    Helper.GetAppDelegate().backend.sendUpdatedInfo("", camOpened: "true", delegate: self)
            }
        }
        completionHandler()
    }
    
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [NSObject : AnyObject], completionHandler: () -> Void)
    {
        NSLog("handleActionWithIdentifier %@ forRemoteNotification %@", identifier!, userInfo)
       // to do
    }
    
    // MARK - ServerWrapper Delegate Methods
    
    func messageTokenSentSuccessfully(results: NSDictionary)
    {
        NSLog("messageTokenSentSuccessfully - \(results)")
    }
    
    func infoUpdatedOnServer(results: NSDictionary)
    {
        NSLog("infoUpdatedOnServer \(results)")
    }
    
    func requestFailed(requestId: Int, errorInfo: NSDictionary)
    {
        NSLog("request failed - \(errorInfo)")
    }
    
    func setRootController()
    {
        if Helper.isUserRegistred()
        {
            if UIApplication.sharedApplication().isRegisteredForRemoteNotifications()
            {
                var settingsScreen = Settings(nibName: "Settings", bundle: nil)
                var navigationContoller = UINavigationController(rootViewController: settingsScreen)
                window?.rootViewController = navigationContoller
            }
            else
            {
                var pushPermissionScreen = PushPermissionScreen(nibName: "PushPermissionScreen", bundle: nil)
                var navigationContoller = UINavigationController(rootViewController: pushPermissionScreen)
                window?.rootViewController = navigationContoller
            }
        }
        else
        {
            var registrationScreen = ViewController(nibName: "ViewController", bundle: nil)
            var navigationContoller = UINavigationController(rootViewController: registrationScreen)
            window?.rootViewController = navigationContoller
        }
    }
    
    func registerForPush()
    {
        if NSUserDefaults.standardUserDefaults().boolForKey(Constants().kPushPermissionKey)
        {
            registerForPushNotifications()
        }
    }
}

