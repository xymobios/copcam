//
//  Helper.swift
//  CopCam
//
//  Created by Nidhi Sharma on 6/2/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit
import AdSupport

public class Helper: NSObject
{
    // returns appdelegate instance
    class func GetAppDelegate() -> AppDelegate
    {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }
    
    // returns device token
    public class func token() -> NSString
    {
        var token = NSString()
        let defaults = NSUserDefaults.standardUserDefaults()
        var loginToken = defaults.objectForKey(Constants().kToken) as? String
        
        if (loginToken != nil && loginToken != "")
        {
            token = loginToken!
        }
        return token
    }
    
    // returns session
    public class func sessionValue() -> NSString
    {
        var session  = NSString()
        let defaults = NSUserDefaults.standardUserDefaults()
        let sessionValue =  defaults.objectForKey(Constants().kSession) as? String
        
        if sessionValue != nil && sessionValue != ""
        {
            session = sessionValue!
        }
        return session
    }
    
    public class func isUserRegistred() -> Bool
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        var loginToken = defaults.objectForKey(Constants().kToken) as? String
        
        if (loginToken != nil && loginToken != "")
        {
            return true
        }
        return false
    }
    
    // returns device platform info
    public class func devicePlatformInformation() ->NSString
    {
        let device = UIDevice.currentDevice()
        let platform = "\(device.systemName) \(device.systemVersion)"
        return platform
    }
    
    // returns device UUID
    public class func deviceId() -> NSString
    {
        var UUIDStr = NSString()
        if ASIdentifierManager.sharedManager().respondsToSelector(Selector("advertisingIdentifier"))
        {
            UUIDStr = ASIdentifierManager.sharedManager().advertisingIdentifier.UUIDString
        }
        
        if UUIDStr.length == 0
        {
            UUIDStr = UIDevice.currentDevice().identifierForVendor.UUIDString
        }
        
        return UUIDStr
    }
    
    // returns application version
    public class func appVersion() -> NSString
    {
        return NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString")as! String
    }
    
    // returns App build Number
    public class func buildNumber() -> NSString
    {
        return NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleVersion") as! String
    }

    // Internet connection warning
    public class func showInternetConnectionWarning()
    {
        showAlertDialog(Constants().kWarning, alertMessage: Constants().kNoInternetConnectionMsg)
    }
    
    // show alert dialog
    public class func showAlertDialog(alertTitle: NSString, alertMessage: NSString)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let title = alertTitle as String
        let msg = alertMessage as String
        
        var alert = UIAlertController(title: title,
            message: msg,
            preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: Constants().kOk, style: UIAlertActionStyle.Default, handler: nil))
        appDelegate.window?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
    }
}
