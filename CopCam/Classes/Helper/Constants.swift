//
//  Constants.swift
//  TrackMyLocation
//
//  Created by Nidhi Sharma on 6/4/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

public struct Constants
{
    //************************  Recipe related ******************************/
    
    // Urls
    let URL_CREATE_ACCOUNT                  =  "http://www.xymob.com/xyapps/account/create"
    let URL_SEND_MSG_TOKEN                  =  "http://www.xymob.com/xyapps/account/msgtoken"
    let URL_SEND_UPDATED_INFO               =  "http://www.xymob.com/xyapps/copcam/update"
    
    // Application keys
    let kToken                              =   "token"
    let kSession                            =   "session"
    let kPlatform                           =   "platform"
    let kDeviceId                           =   "device"
    let kAppVersion                         =   "app-version"
    let kVersion                            =   "version"
    let kAppCode                            =   "app-code"
    
    let kMessage                            =   "message"
    let kCode                               =   "code"
    let kError                              =   "Error"
    let kWarning                            =   "Warning"
    
    let kNotificationTimeInterval           =   "NotificationTimeInterval"
    
    
    // Push permission related
    let kPushPermissionKey                  =   "PushPermissionKey"
    let kAlertPushPermission                =   "We need your Push notifications permission to send you alerts."
    let kEnableNotification                 =   "Enable Push Notifications"
    
    // Server Keys
    let kServerError                        =   "error"
    let kServerEmail                        =   "email"
    let kServerMsg                          =   "msg"
    let kServerStatus                       =   "status"
    
    // Button Names
    let kCancel                             =   "Cancel"
    let kOk                                 =   "Ok"
    let kOpenSettings                       =   "Open Settings"
    
    // Request Time Interval
    let REQUEST_TIME_OUT : NSTimeInterval   =   60
    
    // Http Method Types
    let HTTP_METHOD_GET                     =   "GET"
    let HTTP_METHOD_POST                    =   "POST"
    let HTTP_METHOD_PUT                     =   "PUT"
    let HTTP_METHOD_HEAD                    =   "HEAD"
    let HTTP_METHOD_DELETE                  =   "DELETE"
    let HTTP_METHOD_PATCH                   =   "PATCH"
    
    // Request Ids
    let REQUEST_ID_CREATE_ACCOUNT           =   1
    let REQUEST_ID_SEND_MSG_TOKEN           =   2
    let REQUEST_ID_SEND_UPDATED_INFO        =   3
    
    // Alert Message Strings
    let kNetworkFailed                      =   "Network Failed, please try again later."
    let KEnterFirstName                     =   "Please enter your first name."
    let KEnterLastName                      =   "Please enter your last name."
    let kEnterId                            =   "Please enter your Id."
    let kErrorMessage                       =   "Some error occured, please try again later."
        
    let kNoInternetConnectionMsg            =   "We are unable to connect to the network. Please make sure that your device is connected to the internet."
    
    // Setting Screen Related
    let kAlertTimeUpdated                   =   "Time is updated on server successfully."
    let kAlertEnterTime                     =   "Please enter time in minutes."
    
}