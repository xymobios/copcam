//
//  ServerWrapper.swift
//  CopCam
//
//  Created by Nidhi Sharma on 6/1/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import Foundation

@objc protocol ServerWrapperDelegate
{
    optional func registrationCompleted(results:NSDictionary)
    optional func messageTokenSentSuccessfully(results:NSDictionary)
    optional func infoUpdatedOnServer(results:NSDictionary)
    func requestFailed(requestId : Int, errorInfo : NSDictionary)
}

class ServerWrapper: NSObject, HTTPRequestDelegate
{
    var delegate: ServerWrapperDelegate?
    var requestHeaders = NSMutableDictionary()
    var constants = Constants()
    
    override init()
    {
        // set request Headers values
        
        let devicePlatform = Helper.devicePlatformInformation() as String
        let deviceId = Helper.deviceId() as String
        let appversion = Helper.appVersion() as String

        if !devicePlatform.isEmpty
        {
            requestHeaders.setObject(devicePlatform, forKey: constants.kPlatform)
        }
        if !deviceId.isEmpty
        {
            requestHeaders.setObject(deviceId, forKey: constants.kDeviceId)
        }
        
        if !appversion.isEmpty
        {
            requestHeaders.setObject(appversion, forKey: constants.kAppVersion)
        }
        
        if Helper.isUserRegistred()
        {
            if let session = Helper.sessionValue() as String?
            {
                requestHeaders.setValue(session, forKey: constants.kSession)
            }
            
            let token = Helper.token() as String?
            if token != nil && token != ""
            {
                requestHeaders.setObject(token!, forKey: constants.kToken)
            }
        }
        
        requestHeaders.setObject("1.0", forKey:constants.kVersion)
        
        requestHeaders.setObject("CopCam", forKey: constants.kAppCode)
    }
    
    // create account
    func createAccount(firstname: NSString, lastname: NSString, userId: NSString, delegate:ServerWrapperDelegate)
    {
        var body = "firstname=\(firstname)&lastname=\(lastname)&userId=\(userId)&email=\(userId)@CopCam&password=\(userId)&provider=app"
        var httpBody = body.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)?.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        var urlString = Constants().URL_CREATE_ACCOUNT as String
        var httpMethod = Constants().HTTP_METHOD_POST
        var requestId = Constants().REQUEST_ID_CREATE_ACCOUNT
        self.delegate = delegate
        
        performRequest(urlString, httpMethod: httpMethod, requestId: requestId, headers: requestHeaders, httpBody: httpBody!, delegate: self)
    }
    
    // send push token
    func sendMessageToken(deviceToken: NSData, delegate:ServerWrapperDelegate)
    {
        NSLog("devicetoken \(deviceToken)")
        
        let tokenChars = UnsafePointer<CChar>(deviceToken.bytes)
        var tokenString = ""
        
        for var i = 0; i < deviceToken.length; i++
        {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        NSLog("tokenString: \(tokenString)")
        
        var body = "deviceToken=\(tokenString)"
        var httpBody = body.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)?.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        var urlString = Constants().URL_SEND_MSG_TOKEN as String
        var httpMethod = Constants().HTTP_METHOD_POST
        var requestId = Constants().REQUEST_ID_SEND_MSG_TOKEN
        self.delegate = delegate
        
        performRequest(urlString, httpMethod: httpMethod, requestId: requestId, headers: requestHeaders, httpBody: httpBody!, delegate: self)
    }
    
    func sendUpdatedInfo(remindAfter: NSString, camOpened: NSString, delegate:ServerWrapperDelegate)
    {
        var body = NSString()
        
        if remindAfter.length != 0
        {
            body.stringByAppendingString("remindAfter=\(remindAfter)")
        }
        
        if camOpened.length != 0
        {
            if body.length == 0
            {
                body.stringByAppendingString("camOpened=\(camOpened)")
            }
            else
            {
                body.stringByAppendingString("&camOpened=\(camOpened)")
            }
        }
        
        NSLog("body %@", body)
        var httpBody = body.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)?.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        var urlString = Constants().URL_SEND_UPDATED_INFO as String
        var httpMethod = Constants().HTTP_METHOD_POST
        var requestId = Constants().REQUEST_ID_SEND_UPDATED_INFO
        self.delegate = delegate
        
        performRequest(urlString, httpMethod: httpMethod, requestId: requestId, headers: requestHeaders, httpBody: httpBody!, delegate: self)
    }
    
    func performRequest(url: NSString, httpMethod: NSString, requestId: Int, headers:NSDictionary, httpBody: NSData, delegate: AnyObject) -> HTTPRequest
    {
        var request = HTTPRequest()
        request.url = url
        request.httpMethod = httpMethod
        request.requestId = requestId
        request.postBody = httpBody
        request.delegate = self
        request.performRequest(headers)
        NSLog("Sending Request to %@", url)
        return request
    }
    
    // MARK - HTTPRequest Delegate Methods
    
    func dataReceived(owner: HTTPRequest, results: NSDictionary)
    {
        if results.objectForKey(constants.kServerStatus)?.integerValue == 200
        {
            if owner.requestId == constants.REQUEST_ID_CREATE_ACCOUNT
            {
                if let delegate = self.delegate
                {
                    delegate.registrationCompleted!(results)
                }
            }
            else if owner.requestId == constants.REQUEST_ID_SEND_MSG_TOKEN
            {
                if let delegate = self.delegate
                {
                    delegate.messageTokenSentSuccessfully!(results)
                }
            }
            else if owner.requestId == constants.REQUEST_ID_SEND_UPDATED_INFO
            {
                if let delegate = self.delegate
                {
                    delegate.infoUpdatedOnServer!(results)
                }
            }
        }
        else
        {
            var errorMessage = Constants().kNetworkFailed
            
            if results.objectForKey(constants.kServerError) != nil &&
                results.objectForKey(constants.kServerError)?.objectForKey(constants.kServerMsg) != nil
            {
                errorMessage = results.objectForKey(constants.kServerError)?.objectForKey(constants.kServerMsg) as! String
            }

            var code = results.objectForKey(constants.kServerError)?.objectForKey(constants.kCode) as! Int
            var errorDict : NSDictionary = [constants.kMessage : errorMessage, constants.kCode : code]
            requestFailed(owner, results: errorDict)
        }
    }
    
    func requestFailed(owner: HTTPRequest, results: NSDictionary)
    {
        if let delegate = self.delegate
        {
            delegate.requestFailed(owner.requestId!, errorInfo: results)
        }
    }
}


