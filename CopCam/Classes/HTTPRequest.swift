//
//  HTTPRequest.swift
//  CopCam
//
//  Created by Nidhi Sharma on 6/1/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import Foundation

protocol HTTPRequestDelegate
{
    func requestFailed(owner: HTTPRequest, results:NSDictionary)
    func dataReceived(owner: HTTPRequest, results:NSDictionary)
}

class HTTPRequest: NSObject, NSURLConnectionDataDelegate
{
    var httpMethod = NSString()
    var delegate: HTTPRequestDelegate?
    var receivedData = NSMutableData()
    var requestId : Int?
    var postBody = NSData()
    var url = NSString()

    func performRequest(headers: NSDictionary)
    {
        var urlStr = NSURL(string: url as String)
        var request = NSMutableURLRequest(URL: urlStr!)
        request.HTTPMethod = httpMethod as String
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.timeoutInterval = Constants().REQUEST_TIME_OUT
        
        for (key,val) in headers
        {
            request.setValue(val as? String, forHTTPHeaderField: key as! String)
        }
        request.HTTPBody = postBody
        
        // making request
        var connection  = NSURLConnection(request: request, delegate: self, startImmediately: true)!
        
        receivedData = NSMutableData()
    }
    
    // MARK - NSURLConnection Delegate
    
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse)
    {
        NSLog("didReceiveResponse")
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData)
    {
        NSLog("didReceiveData")
        
        // Appending data
        self.receivedData.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection)
    {
        NSLog("connectionDidFinishLoading")
        
        if let responseDict: NSDictionary = NSJSONSerialization.JSONObjectWithData((self.receivedData), options: NSJSONReadingOptions.MutableContainers, error: nil) as? NSDictionary
        {
            NSLog("response \(responseDict)")
            if let delegate = self.delegate
            {
                 delegate.dataReceived(self, results: responseDict)
            }
        }
        else
        {
            NSLog("response not found")
        }
    }
    
    func connection(connection: NSURLConnection, didFailWithError error: NSError)
    {
        NSLog("didFailWithError %@", error)
        
        var errorMessage = Constants().kNetworkFailed
        
        if !error.localizedDescription.isEmpty
        {
            errorMessage = error.localizedDescription
        }
        
        if let delegate = self.delegate
        {
            var errorDict = [Constants().kMessage : errorMessage, Constants().kCode : error.code]
            delegate.requestFailed(self, results: errorDict)
        }
    }
}