//
//  ModalViewController.swift
//  CopCam
//
//  Created by Nidhi Sharma on 7/13/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

class ModalViewController: UIViewController {

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closePressed(sender: AnyObject)
    {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
