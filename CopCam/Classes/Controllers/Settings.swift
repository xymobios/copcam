//
//  Settings.swift
//  CopCam
//
//  Created by Nidhi Sharma on 7/13/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

class Settings: UIViewController, ServerWrapperDelegate
{
    @IBOutlet var timeField: UITextField!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var progressIndicatingView: UIView!
    
    // MARK - LifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.hidden = true;
        
        if (NSUserDefaults.standardUserDefaults().valueForKey(Constants().kNotificationTimeInterval) != nil)
        {
            timeField.text = NSUserDefaults.standardUserDefaults().objectForKey(Constants().kNotificationTimeInterval) as! String
        }
        submitButton.layer.cornerRadius = 4.0
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK - UIActions
    @IBAction func submitButtonPressed(sender: AnyObject)
    {
        timeField.text = timeField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        if timeField.text.isEmpty
        {
            Helper.showAlertDialog(Constants().kError, alertMessage: Constants().kAlertEnterTime)
            return
        }
        
        progressIndicatingView.hidden = false
        if Helper.GetAppDelegate().isInvokedFromPush == 0
        {
            Helper.GetAppDelegate().backend.sendUpdatedInfo(timeField.text, camOpened:"false", delegate: self)
        }
        else if Helper.GetAppDelegate().isInvokedFromPush == 1 // Invoked from notifications (Remind Me button tapped)
        {
            Helper.GetAppDelegate().backend.sendUpdatedInfo(timeField.text, camOpened:"", delegate: self)
        }
        NSUserDefaults.standardUserDefaults().setObject(timeField.text, forKey: Constants().kNotificationTimeInterval)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    // MARK - ServerWrapperDelegate Methods

    func infoUpdatedOnServer(results: NSDictionary)
    {
        NSLog("infoUpdatedOnServer - \(results)")
        
        progressIndicatingView.hidden = true
        
        if Helper.GetAppDelegate().isInvokedFromPush != 2
        {
            Helper.showAlertDialog(Constants().kError, alertMessage: Constants().kAlertTimeUpdated)
        }
        Helper.GetAppDelegate().isInvokedFromPush = 0
    }
    
    func requestFailed(requestId: Int, errorInfo: NSDictionary)
    {
        NSLog("requestFailed - \(errorInfo)")
        progressIndicatingView.hidden = true
    }
    
    @IBAction func dismissButton(sender: AnyObject)
    {
        self.view.endEditing(true)
    }
}
