//
//  PushPermissionScreen.swift
//  CopCam
//
//  Created by Nidhi Sharma on 7/15/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

class PushPermissionScreen: UIViewController, UIAlertViewDelegate{

    @IBOutlet var yesButton: UIButton!
    
    // MARK - LifeCycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.navigationController?.navigationBar.hidden = true;
        yesButton.layer.cornerRadius = 4.0
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK - UIActions
    @IBAction func notNowPressed(sender: AnyObject)
    {
        Helper.showAlertDialog(Constants().kError, alertMessage:Constants().kAlertPushPermission)
    }
    
    @IBAction func yesButtonPressed(sender: AnyObject)
    {
        if NSUserDefaults.standardUserDefaults().boolForKey(Constants().kPushPermissionKey)
        {
            var alert = UIAlertController(title: Constants().kEnableNotification,
                message: Constants().kAlertPushPermission,
                preferredStyle: UIAlertControllerStyle.Alert)
            
            // add button to open Settings
            alert.addAction(UIAlertAction(title: Constants().kOpenSettings, style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
            }))
            
            // cancel button
            alert.addAction(UIAlertAction(title: Constants().kCancel, style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
            }))
            
            Helper.GetAppDelegate().window?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: Constants().kPushPermissionKey)
            NSUserDefaults.standardUserDefaults().synchronize()
            
            Helper.GetAppDelegate().registerForPushNotifications()
            
            var settingScreen = Settings(nibName:"Settings", bundle: nil)
            self.navigationController?.pushViewController(settingScreen, animated: true)
        }
    }
}

