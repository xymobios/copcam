//
//  ViewController.swift
//  CopCam
//
//  Created by Nidhi Sharma on 7/13/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIAlertViewDelegate, UITextFieldDelegate, ServerWrapperDelegate
{
    var constants = Constants()
    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var idField: UITextField!
    @IBOutlet var registerButton: UIButton!
    @IBOutlet var progressIndicatingView: UIView!
    
    // MARK - View LifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.hidden = true;
        registerButton.layer.cornerRadius = 4.0
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    // MARK - UIActions
    @IBAction func RegisterPressed(sender: AnyObject)
    {
        firstName.text = firstName.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        lastName.text = lastName.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        idField.text = idField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        var alertMessage : NSString = "";
        
        if firstName.text.isEmpty
        {
            alertMessage = constants.KEnterFirstName
        }
        else if lastName.text.isEmpty
        {
            alertMessage = constants.KEnterLastName
        }
        else if idField.text.isEmpty
        {
            alertMessage = constants.kEnterId
        }
        
        if alertMessage.length > 0
        {
            Helper.showAlertDialog(constants.kError, alertMessage: alertMessage)
            return
        }
        
        if Reachability.reachabilityForInternetConnection().isReachable() == false
        {
            Helper.showInternetConnectionWarning()
            return
        }
            
        // send request to server to create account
        Helper.GetAppDelegate().backend.createAccount(firstName.text, lastname: lastName.text, userId: idField.text, delegate: self)
        progressIndicatingView.hidden = false
        self.view.endEditing(true)
    }
    
    @IBAction func dismissKeyboard(sender: AnyObject)
    {
        self.view.endEditing(true)
    }

    
    // MARK - ServerWrapper Delegate Methods
    
    func registrationCompleted(results: NSDictionary)
    {
        NSLog("registration completed \(results)")
        
        progressIndicatingView.hidden = true
        
        if results.objectForKey(constants.kToken) != nil &&
            results.objectForKey(constants.kToken)?.length != 0
        {
            let defaults = NSUserDefaults.standardUserDefaults()
            
            // save session
            if results.objectForKey(constants.kSession) != nil
            {
                defaults.setObject(results.objectForKey(constants.kSession), forKey: constants.kSession)
                Helper.GetAppDelegate().backend.requestHeaders.setValue(results.objectForKey(constants.kSession), forKey: constants.kSession)
            }
            
            // save token
            defaults.setObject(results.objectForKey(constants.kToken), forKey: constants.kToken)
            Helper.GetAppDelegate().backend.requestHeaders.setObject(results.objectForKey(constants.kToken)!, forKey: constants.kToken)
            
            var pushScreen = PushPermissionScreen(nibName:"PushPermissionScreen", bundle: nil)
            navigationController?.pushViewController(pushScreen, animated: true)
        }
        else
        {
            Helper.showAlertDialog(constants.kError, alertMessage:"Token not found.")
        }
    }
    
    func requestFailed(requestId: Int, errorInfo: NSDictionary)
    {
        println("requestFailed with Error \(errorInfo)")
        
        progressIndicatingView.hidden = true
        
        // Show alert dialog
        var errMsg = errorInfo.objectForKey(constants.kMessage) as! String
        Helper.showAlertDialog(constants.kError, alertMessage: errMsg)
    }
}

